$(function () {
    $("#newtc-btn").on("click", function () {
        $("#newtc").toggle('slow');
    });

    $("#tc-form-submit").on("click", function () {
        $.ajax({
            method: "POST",
            url: '/survey/addNewTc',
            data: $("#tc-form").serialize(),
            success: function (data) {
                if(data.success) {
                    window.location.reload();
                }
            }
        });
    });

    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-1y'
    });

    // $("#login-form-submit").on("click", function () {
    //     $("#login-form .form-error").hide(200);
    //     $("#login-form .form-error").remove();
    //     $.ajax({
    //         method: "POST",
    //         url: '/login',
    //         data: $("#login-form").serialize(),
    //         success: function (data) {
    //             if (data.success) {
    //                 window.location.href = '/user/training-Cycle';
    //             } else {
    //                 if (data.errors.constructor === Object) {
    //                     $.each(data.errors, function (k, v) {
    //                         $('#login-form').find('input[name="' + k + '"]').parent().append('<div class="form-error" style="display: none"><i class="fa fa-exclamation-triangle"></i> ' + Object.values(v) + '</div>');
    //                     });
    //                 } else {
    //                     $('#login-form').find('.login-error').html('<i class="fa fa-exclamation-triangle"></i> ' + data.errors[0]).show(200);
    //                 }
    //                 $("#login-form .form-error").show(200);
    //             }
    //         }
    //     })
    // });
    $('#login-form input').click(function () {
        $(this).parent().find('.form-error').hide(200).remove();
    });
    $('#registration-form input').click(function () {
        $(this).parent().find('.form-error').hide(200).remove();
    });
    $(".tc").each(function () {
        var dataId = $(this).attr("data-id");
        $(this).on("click", function () {
            window.location = "/survey/get/" + dataId;
        });
    });


    $("#survey-form-submit").on("click", function () {
        $.ajax({
            method: "POST",
            url: '/survey/addSurveyAnswers',
            data: $("#survey-form").serialize(),
            success: function (data) {
                if(data.success == true && data.byInvitation == true) {
                    window.location = "/thanks";
                }
                if(data.success == true && data.byInvitation != true) {
                    window.location = "/user/profile";
                }
            }
        })
    });

});
