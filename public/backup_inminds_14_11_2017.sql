-- --------------------------------------------------------
-- Host:                         vm5.danmar.com
-- Wersja serwera:               5.5.58 - MySQL Community Server (GPL) by Remi
-- Serwer OS:                    Linux
-- HeidiSQL Wersja:              9.3.0.4989
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela inminds_dcnet_eu.survey
CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `questions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.survey: ~4 rows (około)
/*!40000 ALTER TABLE `survey` DISABLE KEYS */;
INSERT INTO `survey` (`id`, `title`, `level`, `questions`, `description`) VALUES
	(1, 'First Survey', 1, '["What is your overall assessment of the program?","Was the teaching method effective?","Was the program relevant to your needs?","Was the content appropriate?"]', 'Measures your reaction to and satisfaction with the training program and your plans for action '),
	(2, 'Second Survey', 2, '{"IF YOU HAVE ATTENDED CUSTOMER SERVICE MODULE PLEASE ANSWER THE FOLLOWING QUESTIONS":[{"Which of the following are behaviors?":[{"Smile":true},{"Kindness":false},{"Establish eye contact":true},{"Friendliness":false},{"Identify yourself":true}]},{"Which of the following are open directed questions?":[{"What do you like at our products?":true},{"Would you like to call you tomorrow?":false},{"Are you not satisfied with the solution I suggest?":false},{"What time should I call you again?":true},{"What do you think about our services?":false}]},{"In what phases of the complaint structure we have the role of a doctor?":[{"Hallo":false},{"Active listening":true},{"Goodbye":false},{"Questions":true},{"Solution":false}]}],"IF YOU HAVE ATTENDED THE DEVELOPMENT MODULE PLEASE ANSWER THE FOLLOWING QUESTIONS":[{"Which of the following are steps in the \\"Decision announcement\\"?":[{"Silence":true},{"No opening":false},{"Lot of questions":false},{"Explanation":true},{"Answering questions":false}]},{"Which are the two attidues of manager?":[{"Participative":true},{"Directive":true},{"Neutral":false},{"Explanative":false},{"Negative":false}]},{"Which of the following are steps of the appraisal system interview?":[{"Welcome":true},{"Without preparation":false},{"Action plan":true},{"Analytical opening":false},{"Go back only the positive past":false}]}],"IF YOU HAVE ATTENDED THE TIME MANAGEMENT MODULE PLEASE ANSWER THE FOLLOWING QUESTIONS":[{"What\'s included in time management?":[{"Delegation of responsibilities":true},{"Solving all the problems":false},{"Setting deadlines":true},{"No saying no":false},{"Setting priorities":false}]},{"Which of the following are important?":[{"Answering the phone":false},{"Going to the dentist regularly":true},{"Reading funny emails ":false},{"Finishing an important project - Deadline of the project is next month":true},{"Finishing a task-Deadline of the task is within next hour":true}]}],"IF YOU HAVE ATTENDED THE DESIGN THINKING PLEASE ANSWER THE FOLLOWING QUESTIONS":[{"Which of the following are phases of Design thinking?":[{"Inspire":true},{"Design":false},{"Benchmarking":false},{"Implement":true},{"Ideate":true}]},{"How is inspiration achieved?":[{"Observe":true},{"Craft \\u201cHow Might We\\u201d Questions":false},{"Engage":true},{"Watch and Listen":true},{"Build on the ideas of others":false}]},{"Which of the following grossery consumers are extreme users?":[{"Mother of one":false},{"Athlete":true},{"Vegetarian":true},{"Couple":false},{"Middle - aged man":false}]},{"How we should engage extreme users?":[{"Determine who is extreme user":true},{"Look everywhere for extreme user":false},{"Engage":true},{"Interviews":false},{"Do not focus on behaviors":false}]}]}', 'Measures skills and knowledge gains'),
	(3, 'Third Survey', 3, '{" ":[{"To what degree have you applied what you have learned?":[""]}],"IF YOU HAVE ATTENDED CUSTOMER SERVICE MODULE PLEASE ANSWER THE FOLLOWING QUESTION":[{"Which techniques have you found more useful in practice. Please rank":["The hallo technique","The goodbye technique","Playing the role of the doctor","How to say \\"no\\"","Types of questions"]}],"IF YOU HAVE ATTENDED THE DEVELOPMENT MODULE PLEASE ANSWER THE FOLLOWING QUESTION":[{"Which techniques have you found more useful in practice. Please rank":["The performance appraisal interview","Problem solving meeting","The two attitudes of manager","The reprimand interview","One-to-one meeting"]}],"IF YOU HAVE ATTENDED THE TIME MANAGEMENT MODULE PLEASE ANSWER THE FOLLOWING QUESTION":[{"Which techniques have you found more useful in practice. Please rank":["Understanding The Difference Between Urgent and Important","Effective Planning","Delegation of responsibilities","Setting goals and objectives","Setting deadlines"]}],"IF YOU HAVE ATTENDED THE DESIGN THINKING MODULE PLEASE ANSWER THE FOLLOWING QUESTION":[{"Which techniques have you found more useful in practice. Please rank":["Methodology of Inspiration","Methodology of Ideation","Methodology of Implementation","Methodology of Evaluation","Recognising Extreme Users"]}],"  ":[{"To what degree have your collegues recognised the changes of your behavior?":["  "]}],"   ":[{"To what degree have your clients recognised the changes of your behavior?":["   "]}]}', 'Measures changes in on-the-job application, behaviour change, and implementation'),
	(4, 'Fourth Survey', 4, '["Did you see any difference in your business results? To  what degree do you estimate that the acquired beahaviors have influenced results","To what degree the learning have influenced your effectiveness/ Your productivity"," How much your internal and/or external client satisfaction has beeen increased (Number of customer complaints internal-external)","Are your colleagues satisfied?"]', 'Final results, business results, any outcome that most people would agree is "good for the business"');
/*!40000 ALTER TABLE `survey` ENABLE KEYS */;


-- Zrzut struktury tabela inminds_dcnet_eu.survey_answers
CREATE TABLE IF NOT EXISTS `survey_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tc_id` int(11) NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `filled_via_invitation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_14FCE5BDB3FE509D` (`survey_id`),
  CONSTRAINT `FK_14FCE5BDB3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.survey_answers: ~67 rows (około)
/*!40000 ALTER TABLE `survey_answers` DISABLE KEYS */;
INSERT INTO `survey_answers` (`id`, `survey_id`, `user_id`, `tc_id`, `answer`, `filled_via_invitation`) VALUES
	(2, 1, 94, 31, '{"input-1":"2","input-2":"3","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(4, 1, 21, 19, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(5, 2, 21, 19, '{"input-1-1":["3","5"],"input-1-2":["1","3","5"],"input-1-3":["2","4"],"input-2-1":["4","5"],"input-2-2":["2","4"],"input-2-3":["3","5"],"input-3-1":["2","3"],"input-3-2":["1","4"],"input-4-1":["2","4","5"],"input-4-2":["3","4"],"input-4-3":["3","4"],"input-4-4":["4","5"],"comments":[""]}', '0'),
	(6, 3, 21, 19, '{"input-1-1":"4","input-2-1":"3","input-2-2":"3","input-2-3":"5","input-2-4":"5","input-3-1":"5","input-3-2":"3","input-3-3":"3","input-3-4":"4","input-6-1":"4","input-7-1":"3","comments":[""]}', '0'),
	(7, 4, 21, 19, '{"input-1":"5","input-2":"3","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(8, 1, 11, 27, '{"input-1":"5","input-2":"4","input-3":"3","input-4":"2","comments":[""]}', '0'),
	(9, 2, 11, 27, '{"input-1-1":["3","4","5"],"input-1-2":["2","3"],"input-1-3":["2","3"],"input-2-1":["2","4"],"input-2-2":["4"],"input-2-3":["2"],"input-3-1":["2","3"],"input-3-2":["4"],"input-4-1":["1","2","3","5"],"input-4-2":["2","5"],"input-4-3":["3"],"input-4-4":["1","5"],"comments":[""]}', '0'),
	(10, 1, 11, 28, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(11, 2, 11, 28, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(12, 3, 11, 28, '{"input-1-1":"5","input-2-1":"4","input-2-2":"2","input-2-3":"4","input-2-4":"4","input-6-1":"5","input-7-1":"5","comments":[""]}', '0'),
	(13, 1, 22, 20, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(14, 2, 22, 20, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(15, 3, 22, 20, '{"input-1-1":"4","input-2-1":"3","input-2-2":"3","input-2-3":"5","input-2-4":"5","input-3-1":"5","input-3-2":"3","input-3-3":"3","input-3-4":"4","input-6-1":"4","input-7-1":"3","comments":[""]}', '0'),
	(16, 4, 22, 20, '{"input-1":"5","input-2":"3","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(17, 1, 23, 21, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(18, 2, 23, 21, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(19, 3, 23, 21, '{"input-1-1":"5","input-2-1":"4","input-2-2":"2","input-2-3":"4","input-2-4":"4","input-6-1":"5","input-7-1":"5","comments":[""]}', '0'),
	(20, 4, 23, 21, '{"input-1":"5","input-2":"3","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(21, 1, 23, 22, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(22, 2, 23, 22, '{"input-1-1":["3","4","5"],"input-1-2":["2","3"],"input-1-3":["2","3"],"input-2-1":["2","4"],"input-2-2":["4"],"input-2-3":["2"],"input-3-1":["2","3"],"input-3-2":["4"],"input-4-1":["1","2","3","5"],"input-4-2":["2","5"],"input-4-3":["3"],"input-4-4":["1","5"],"comments":[""]}', '0'),
	(23, 1, 24, 24, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(24, 2, 24, 24, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(25, 1, 99, 27, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"3","comments":["need improve some modules"]}', '0'),
	(26, 2, 99, 27, '{"input-1-1":["2","4"],"input-1-2":["3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(27, 1, 100, 20, '{"input-1":"3","input-2":"3","input-3":"4","input-4":"3","comments":[""]}', '0'),
	(28, 2, 100, 20, '{"input-3-1":["1","3"],"input-3-2":["4","5"],"comments":[""]}', '0'),
	(29, 3, 100, 20, '{"input-1-1":"4","input-4-1":"4","input-4-2":"3","input-4-3":"4","input-4-4":"5","input-6-1":"4","input-7-1":"4","comments":[""]}', '0'),
	(30, 4, 100, 20, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"2","comments":[""]}', '0'),
	(31, 1, 101, 21, '{"input-1":"5","input-2":"4","input-3":"2","input-4":"2","comments":[""]}', '0'),
	(32, 2, 101, 21, '{"input-2-1":["1","3","4"],"input-2-2":["2","3"],"input-2-3":["2","5"],"comments":[""]}', '0'),
	(33, 3, 101, 21, '{"input-1-1":"3","input-3-1":"5","input-3-2":"4","input-3-3":"2","input-3-4":"4","input-6-1":"3","input-7-1":"4","comments":[""]}', '0'),
	(34, 4, 101, 21, '{"input-1":"4","input-2":"3","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(35, 1, 102, 31, '{"input-1":"5","input-2":"5","input-3":"2","input-4":"4","comments":[""]}', '0'),
	(36, 2, 102, 31, '{"input-1-1":["1","4"],"input-1-2":["3","4"],"input-1-3":["2","4"],"comments":[""]}', '0'),
	(37, 1, 103, 20, '{"input-1":"3","input-2":"3","input-3":"4","input-4":"3","comments":[""]}', '0'),
	(38, 2, 103, 20, '{"input-4-1":["2","4"],"input-4-2":["1","3","4"],"input-4-3":["2","3"],"input-4-4":["2","5"],"comments":[""]}', '0'),
	(39, 3, 103, 20, '{"input-1-1":"4","input-5-1":"4","input-5-2":"5","input-5-3":"3","input-5-4":"3","input-6-1":"3","input-7-1":"2","comments":[""]}', '0'),
	(40, 4, 103, 20, '{"input-1":"3","input-2":"3","input-3":"2","input-4":"4","comments":[""]}', '0'),
	(41, 1, 104, 22, '{"input-1":"4","input-2":"5","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(42, 2, 104, 22, '{"input-2-1":["3","4","5"],"input-2-2":["2","4"],"input-2-3":["3","4"],"comments":[""]}', '0'),
	(43, 1, 14, 19, '{"input-1":"5","input-2":"3","input-3":"3","input-4":"4","comments":[""]}', '0'),
	(44, 1, 1, 15, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(45, 1, 7, 14, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(46, 2, 7, 14, '{"input-1-1":["3","5"],"input-1-2":["1","3","5"],"input-1-3":["2","4"],"input-2-1":["4","5"],"input-2-2":["2","4"],"input-2-3":["3","5"],"input-3-1":["2","3"],"input-3-2":["1","4"],"input-4-1":["2","4","5"],"input-4-2":["3","4"],"input-4-3":["3","4"],"input-4-4":["4","5"],"comments":[""]}', '0'),
	(47, 3, 7, 14, '{"input-1-1":"4","input-2-1":"3","input-2-2":"3","input-2-3":"5","input-2-4":"5","input-3-1":"5","input-3-2":"3","input-3-3":"3","input-3-4":"4","input-6-1":"4","input-7-1":"3","comments":[""]}', '0'),
	(48, 1, 8, 14, '{"input-1":"5","input-2":"4","input-3":"3","input-4":"2","comments":[""]}', '0'),
	(49, 2, 8, 14, '{"input-1-1":["3","4","5"],"input-1-2":["2","3"],"input-1-3":["2","3"],"input-2-1":["2","4"],"input-2-2":["4"],"input-2-3":["2"],"input-3-1":["2","3"],"input-3-2":["4"],"input-4-1":["1","2","3","5"],"input-4-2":["2","5"],"input-4-3":["3"],"input-4-4":["1","5"],"comments":[""]}', '0'),
	(50, 3, 8, 4, '{"input-1-1":"5","input-2-1":"3","input-2-2":"3","input-2-3":"5","input-2-4":"5","input-3-1":"5","input-3-2":"5","input-3-3":"3","input-3-4":"4","input-4-1":"5","input-4-2":"4","input-4-3":"5","input-4-4":"4","input-5-1":"5","input-5-2":"4","input-5-3":"5","input-5-4":"2","input-6-1":"2","input-7-1":"3","comments":[""]}', '0'),
	(51, 1, 10, 9, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(52, 2, 10, 9, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(53, 1, 15, 30, '{"input-1":"5","input-2":"4","input-3":"3","input-4":"2","comments":[""]}', '0'),
	(54, 2, 15, 30, '{"input-1-1":["3","5"],"input-1-2":["1","3","5"],"input-1-3":["2","4"],"input-2-1":["4","5"],"input-2-2":["2","4"],"input-2-3":["3","5"],"input-3-1":["2","3"],"input-3-2":["1","4"],"input-4-1":["2","4","5"],"input-4-2":["3","4"],"input-4-3":["3","4"],"input-4-4":["4","5"],"comments":[""]}', '0'),
	(55, 3, 15, 30, '{"input-1-1":"5","input-2-1":"4","input-2-2":"2","input-2-3":"4","input-2-4":"4","input-6-1":"5","input-7-1":"5","comments":[""]}', '0'),
	(56, 1, 17, 29, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(57, 1, 18, 26, '{"input-1":"5","input-2":"4","input-3":"3","input-4":"2","comments":[""]}', '0'),
	(58, 1, 19, 26, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(59, 1, 27, 26, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(60, 2, 18, 26, '{"input-1-1":["3","5"],"input-1-2":["1","3","5"],"input-1-3":["2","4"],"input-2-1":["4","5"],"input-2-2":["2","4"],"input-2-3":["3","5"],"input-3-1":["2","3"],"input-3-2":["1","4"],"input-4-1":["2","4","5"],"input-4-2":["3","4"],"input-4-3":["3","4"],"input-4-4":["4","5"],"comments":[""]}', '0'),
	(61, 2, 19, 26, '{"input-1-1":["3","4","5"],"input-1-2":["2","3"],"input-1-3":["2","3"],"input-2-1":["2","4"],"input-2-2":["4"],"input-2-3":["2"],"input-3-1":["2","3"],"input-3-2":["4"],"input-4-1":["1","2","3","5"],"input-4-2":["2","5"],"input-4-3":["3"],"input-4-4":["1","5"],"comments":[""]}', '0'),
	(62, 2, 27, 26, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(63, 1, 2, 30, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(64, 2, 2, 30, '{"input-1-1":["2","5"],"input-1-2":["2","3","5"],"input-1-3":["2","5"],"comments":[""]}', '0'),
	(65, 3, 2, 30, '{"input-1-1":"5","input-2-1":"4","input-2-2":"2","input-2-3":"4","input-2-4":"4","input-6-1":"5","input-7-1":"5","comments":[""]}', '0'),
	(66, 1, 9, 15, '{"input-1":"5","input-2":"4","input-3":"3","input-4":"2","comments":[""]}', '0'),
	(67, 1, 13, 15, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0'),
	(68, 1, 16, 15, '{"input-1":"4","input-2":"2","input-3":"3","input-4":"3","comments":[""]}', '0'),
	(69, 1, 26, 25, '{"input-1":"5","input-2":"4","input-3":"4","input-4":"4","comments":[""]}', '0');
/*!40000 ALTER TABLE `survey_answers` ENABLE KEYS */;


-- Zrzut struktury tabela inminds_dcnet_eu.tc2user
CREATE TABLE IF NOT EXISTS `tc2user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tc_id` (`tc_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tc2user_ibfk_1` FOREIGN KEY (`tc_id`) REFERENCES `training_cycle` (`id`),
  CONSTRAINT `tc2user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.tc2user: ~19 rows (około)
/*!40000 ALTER TABLE `tc2user` DISABLE KEYS */;
INSERT INTO `tc2user` (`id`, `tc_id`, `user_id`) VALUES
	(7, 27, 99),
	(9, 20, 100),
	(10, 21, 101),
	(11, 24, 101),
	(12, 21, 99),
	(13, 10, 19),
	(14, 31, 102),
	(15, 20, 103),
	(16, 22, 104),
	(17, 15, 1),
	(20, 14, 7),
	(21, 14, 8),
	(22, 9, 10),
	(23, 30, 15),
	(24, 29, 17),
	(26, 26, 18),
	(27, 26, 19),
	(28, 26, 27),
	(30, 15, 9);
/*!40000 ALTER TABLE `tc2user` ENABLE KEYS */;


-- Zrzut struktury tabela inminds_dcnet_eu.training_cycle
CREATE TABLE IF NOT EXISTS `training_cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `c_invitation` longtext COLLATE utf8_unicode_ci NOT NULL,
  `m_invitation` longtext COLLATE utf8_unicode_ci NOT NULL,
  `blocked` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL DEFAULT '[1,1,1,1]',
  PRIMARY KEY (`id`),
  KEY `IDX_5F5C5BFEA76ED395` (`user_id`),
  CONSTRAINT `FK_5F5C5BFEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.training_cycle: ~20 rows (około)
/*!40000 ALTER TABLE `training_cycle` DISABLE KEYS */;
INSERT INTO `training_cycle` (`id`, `user_id`, `name`, `description`, `date_from`, `date_to`, `c_invitation`, `m_invitation`, `blocked`) VALUES
	(9, 10, 'First training cycle TEST', '[TRAINING CYCLE DESCRIPTION] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec est ante. Curabitur viverra facilisis nulla, et laoreet lacus vehicula in. Duis at volutpat nunc, eleifend tempor lorem. Donec feugiat tortor convallis vehicula malesuada. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In blandit eleifend nisi, eu facilisis elit fringilla eu. Nulla facilisi. Donec quis dolor et odio condimentum tincidunt ut vel sapien. Phasellus nisl nibh, maximus in aliquam at, pharetra vel mi. Duis imperdiet turpis eu mattis ultrices. Donec libero neque, lobortis eleifend blandit non, dignissim non lacus. Morbi condimentum sit amet neque vel sollicitudin.', '2016-12-05', '2017-02-24', 'c58524cdf298e5', 'm58524cdf298de', '[0,0,1,1]'),
	(10, 10, 'Second training cycle TEST', 'This is just another test for creating a new training cycle', '2016-12-01', '2016-12-31', 'c5852c32dc3aa4', 'm5852c32dc3a52', '[1,1,1,1]'),
	(11, 10, '3rd training cycle', '', '2016-12-01', '2016-12-31', 'c5857ed628fc25', 'm5857ed628fbcd', '[1,1,1,1]'),
	(12, 10, 'Mattheos', 'example training cycle', '2017-06-16', '2017-06-23', 'c5942a8c945d2a', 'm5942a8c945cec', '[1,1,1,1]'),
	(13, 10, 'ninetta', 'ninetta', '2017-05-01', '2017-05-11', 'c59439df062bf9', 'm59439df062ba3', '[1,1,1,1]'),
	(14, 10, 'Customer service training', 'The training was organised by XYZ', '2017-06-01', '2017-06-06', 'c5949155edf1ed', 'm5949155edf19d', '[0,0,0,1]'),
	(15, 14, '111', '111', '2017-07-27', '2017-07-26', 'c59772fb355bb1', 'm59772fb355b5d', '[0,1,1,1]'),
	(16, 17, '1111', '111', '2017-07-27', '2017-07-18', 'c59773cdd48dae', 'm59773cdd48d5b', '[1,1,1,1]'),
	(19, 21, 'Test', 'this is a test', '2017-08-03', '2017-08-11', 'c598330b04beab', 'm598330b04be57', '[1,1,1,1]'),
	(20, 22, 'Professional Development', '', '2017-08-03', '2017-08-10', 'c5983313e778a1', 'm5983313e77849', '[0,0,0,0]'),
	(21, 23, '1st Learning Cycle', 'Professional Development', '2017-08-01', '2017-08-04', 'c599fe43daa939', 'm599fe43daa8e7', '[0,0,0,0]'),
	(22, 23, '2nd Learning Cycle', 'Customer Service', '2017-08-14', '2017-08-18', 'c599fe45b9be74', 'm599fe45b9be23', '[0,0,1,1]'),
	(24, 24, 'aaa', 'aaa', '2017-08-25', '2017-08-31', 'c59a03b5c452ac', 'm59a03b5c4524f', '[0,0,1,1]'),
	(25, 26, 'adf', 'asdfsdf', '2017-08-16', '2017-08-31', 'c59a3c6aacc0b5', 'm59a3c6aacc047', '[0,1,1,1]'),
	(26, 24, '1st Cycle - Professional Development', ' Professional Development module', '2017-08-01', '2017-08-04', 'c59a422503be86', 'm59a422503be15', '[0,0,1,1]'),
	(27, 11, 'Training Cycle 1 - Professional Development', 'Complete the professional development module', '2017-09-04', '2017-09-08', 'c59a46e33742df', 'm59a46e3374295', '[0,0,1,1]'),
	(28, 11, 'Training Cycle 2 - Customer Service', 'Complete the customer service module', '2017-09-18', '2017-09-21', 'c59a46e5089527', 'm59a46e50894cc', '[0,0,0,1]'),
	(29, 94, 'Test', 'adf', '2017-10-10', '2017-10-26', 'c59f34730afcaf', 'm59f34730afc53', '[0,1,1,1]'),
	(30, 2, '1st Cycle - Professional Development', 'Professional Development', '2017-10-01', '2017-10-31', 'c59f351fc55017', 'm59f351fc54fa5', '[0,0,0,1]'),
	(31, 94, 'First training cycle', 'This is a sample training cycle', '2017-04-02', '2017-10-20', 'c59f4dbf6dac15', 'm59f4dbf6dabba', '[0,0,1,1]');
/*!40000 ALTER TABLE `training_cycle` ENABLE KEYS */;


-- Zrzut struktury tabela inminds_dcnet_eu.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_account` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `activation_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'NEVER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.user: ~31 rows (około)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `login`, `password`, `firstname`, `lastname`, `gender`, `age`, `company`, `position`, `type_account`, `status`, `activation_hash`, `date_created`, `last_login`) VALUES
	(1, 'testowy', '$2y$10$5GbC4kREyVqoG4tCHsN78O/gokzMBexlLalq58V7bCnQwnALlBMiW', 'John', 'Doe', 'Male', '18 - 27', 'UK', 'Sales', 'User', 1, 'f86bdb19deb2c5ab632734b8d884ce06', '2016-12-07 15:02:36', 'NEVER'),
	(2, 'test@test.pl', '$2y$10$pwgQhxtusizEQQnFDBBjWuiBN.VOMk29CUmyOX5W7kNyeUfEa0RHa', 'xxx', 'xxx', 'Male', '18 - 27', 'Germany', 'Administration', 'Manager', 1, 'e32bd13e2add097461cb96824b7a829c', '2017-08-09 09:42:00', 'NEVER'),
	(7, '123123123123', '$2y$10$kS9kwZZzKBVijpdv.Lvsl.FFHpzOurWxDHmxQGgeA7MXt12jW0vKe', '21312321312', '3213123213', 'Male', '28 - 37', 'UK', 'Sales', 'User', 0, '101193d7181cc88340ae5b2b17bba8a1', '2016-12-08 11:53:22', 'NEVER'),
	(8, 'sdsadsa', '$2y$10$hQigeNRRqvyXPbi2f2XtXu220YnNck78SfuhATr9WgDdYx4hpwHAa', 'dsadsada', 'dda', 'Male', '38 - 47', 'UK', 'Administration', 'User', 0, '942656fd05b5b2da42ef45433ac64f1d', '2016-12-08 11:58:05', 'NEVER'),
	(9, 'asdasd', '$2y$10$cE.Dh8oOcph0Dw1jpFDeReO5/ehqkU6SC8Yogp9zosEa1DhF59vSa', 'sadasd', 'adsad', 'Male', '18 - 27', 'UK', 'Administration', 'Manager', 0, 'a8f5f167f44f4964e6c998dee827110c', '2016-12-08 15:37:46', 'NEVER'),
	(10, 'demo@example.com', '$2y$10$eiXOdYGySfhOeE8EFr3x5OCD3wxgMolvZ/TGzjcUas5nrlHiwVMaO', 'demo', 'demo', 'Male', '18 - 27', 'Poland', 'Sales', 'User', 1, '7c4ff521986b4ff8d29440beec01972d', '2016-12-13 09:11:04', 'NEVER'),
	(11, 'mattheos@kattare.com', '$2y$10$uxTWxM4cYDBRzjJLgkkJaOjsrWxpSBC4PbLeuWTjEhBjYv.d4mdmm', 'Mattheos', 'Kakaris', 'Male', '18 - 27', 'UK', 'Sales', 'Manager', 1, '44ee9d0d9e15c1386dd6225642e3ca48', '2017-06-15 16:56:12', 'NEVER'),
	(13, '--berlin@itkam.org', '$2y$10$vWEzPpOzpr6x/nGNxc5EV.lr5dTVhJoTh2nky8LREfSUZhwFrW86q', 'ITKAM', 'ITKAM', 'Female', '18 - 27', 'UK', 'Management', 'Manager', 0, '3cbe675ce22f78532de0cea338495fe4', '2017-07-24 12:09:45', 'NEVER'),
	(14, '111@example.com', '$2y$10$9pPi8cELFY9t3Ppyq2VCf.naBVCu72b9yVmKcqSZJYAb8Bxu3aOaO', 'TEST', 'TEST', 'Male', '58 <', 'UK', 'Management', 'User', 1, '3063eb1bbd1adf2c1612ded0c2ace4bc', '2017-07-25 13:45:52', 'NEVER'),
	(15, 'seba@example.com', '$2y$10$H.0sh65qEN9vXhOe6bNQuutowLx0YNOSgZ6HAHxadWoJex3dpvgYu', 'aaaa', 'aaaa', 'Male', '18 - 27', 'Poland', 'Production', 'User', 0, '6159dfe7f46f40b3b6a7caec72206d83', '2017-07-25 14:35:33', 'NEVER'),
	(16, 'seba1@example.com', '$2y$10$8F8qCghO6NTUOym1c2KBBeOi2E76zGJp9fgma4S3MFliyLA.v7P.K', 'aaaa', 'aaaa', 'Male', '18 - 27', 'Poland', 'Production', 'Manager', 0, '5842d06e0af16c3821629bd2cc709311', '2017-07-25 14:35:54', 'NEVER'),
	(17, 'xxx123@example.com', '$2y$10$ePaah3laEbVEwHQeXNZRCOlHLSo5KhKIAqwV/TOnFfnT6Cj1.uvYa', '111', '111', 'Male', '18 - 27', 'UK', 'Sales', 'User', 1, '276fa53507cdfc88fb7b993767f7d8a3', '2017-07-25 14:41:36', 'NEVER'),
	(18, 'prink1940@fleckens.hu', '$2y$10$J2yBaM8toQljooahGjJpqOOLhZkxGNUX1VFzzOZa42LIuebhK/yUO', 'Kanio', 'Test', 'Male', '18 - 27', 'Poland', 'Administration', 'User', 1, 'abbd3ba56ef7399c65cf5587619bcbf1', '2017-07-25 23:38:08', 'NEVER'),
	(19, 'krzych11-85@o2.pl', '$2y$10$pOFOMAht10T0AmVUExnOHeX.rMkNgbJuargeBcdFV0US28gq0iwjW', 'krz', 'tom', 'Male', '58 <', 'Italy', 'Sales', 'User', 1, 'd195b7361af41e533e00a9c4ae8b469d', '2017-08-01 11:34:54', 'NEVER'),
	(20, 'berlin@itkam.org', '$2y$10$d0B/I7xuSyWpAl2NpSp2k.kvL7IFVJYPTqgXtB0BHgIPIKeGlp.RK', 'Vincenza ', 'DÂ´Ambrogio', 'Female', '18 - 27', 'UK', 'Management', 'Manager', 1, '3cbe675ce22f78532de0cea338495fe4', '2017-08-02 17:24:21', 'NEVER'),
	(21, 'bogun@satzservice.de', '$2y$10$6RQd1ps3TR2OK6lYjH8BvuaWpr62iIXwho2TV3mU2gd.di3fU2HBq', 'Ulrich', 'Bogun', 'Male', '18 - 27', 'UK', 'General Management', 'Manager', 1, 'eb518a916398d25a29fbb14c9643e924', '2017-08-03 16:10:06', 'NEVER'),
	(22, 'friedrich-rose@t-online.de', '$2y$10$IeJakRsyUYHt62FyApukdOyA0YHW8YjfSByfdVZLZTx79z/M4Vxxu', 'Friedrich', 'Rose', 'Male', '18 - 27', 'UK', 'Management', 'Manager', 1, '36515642fc0a2d664c491ea281d0640f', '2017-08-03 16:18:27', 'NEVER'),
	(23, 'mka@mycoin.eu', '$2y$10$IH5pC.GjCaSBQX.34t0XZeWodFejg69J0ql0ZoYaDgoGNh/oZTHu6', 'Mattheos', 'Kakaris', 'Male', '18 - 27', 'UK', 'Management', 'Manager', 1, '815524d2c2510e121bb40a86e42f0d40', '2017-08-25 10:47:07', 'NEVER'),
	(24, 'test@test.pl', '$2y$10$2xj1hmPAchOPLqq4zcNxNOnMAblfcxdE1LOefducvOLjnXI0z3Tpa', 'test2', 'test2', 'Male', '18 - 27', 'Germany', 'Administration', 'Manager', 1, 'e32bd13e2add097461cb96824b7a829c', '2017-08-25 16:59:14', 'NEVER'),
	(26, 'l.klapa@danmar-computers.com.pl', '$2y$10$UkdMDNHSMcPRiGB8r/lSZ.xn5OpemE7mYcCXKcwMrRyqP1hQnSEh6', 'Åukasz', 'KÅ‚apa', 'Male', '28 - 37', 'Poland', 'Administration', 'Manager', 1, '3ac782b154feedc35fe172e3c54660c6', '2017-08-28 09:28:26', 'NEVER'),
	(27, 'tomaszkordas@outlook.com', '$2y$10$eBVEvLiCyxbWDf7xZ/llGOvhSuj9C8Pbtm6qsERm92TZY2kiY6m6G', 'tomasz', 'kordas', 'Male', '28 - 37', 'Poland', 'Management', 'User', 1, '464ca3a238810d1c5d6102e3a6465896', '2017-09-08 08:33:57', 'NEVER'),
	(28, 'test@tesr2.pl', '$2y$10$/BZnz.xd.SAkwIdLhkzgjOSOO2XjO3g5tubXOXRkUWc.DxSF5lp9y', 'test3', 'test3', 'Male', '28 - 37', 'Greece', 'General Management', 'User', 1, '005fd707d187d7186722a114fdf50347', '2017-10-17 09:18:40', 'NEVER'),
	(94, 'l.klapa@gmail.com', '$2y$10$GtnMJ3LS1HyDHS6.J8Ts7uCrnvPReN3jHk52q.EuZvZKbThsO4rfm', 'Åukasz', 'KÅ‚apa', 'Male', '28 - 37', 'Poland', 'Management', 'Manager', 1, '168c96fde68546acce2fdfe95fac8aa3', '2017-10-27 16:45:42', 'CREATED'),
	(98, 'l.klap.a@gmail.com', '$2y$10$X1WPWG556GEyQzcdw5t4eu65HLX8fjvys7SEe78rB9czMA8sy3O6e', 'aaa', 'aa', 'Male', '28 - 37', 'Poland', 'Management', 'Manager', 1, '1ab42d8bfc01edc03232dfbd8c5bfbe5', '2017-10-31 15:38:33', 'CREATED'),
	(99, 'John.doe@wp.pl', '$2y$10$pQxtbMuuFSca/pe4NDzQ3uxTt8BKiS0/X6auBmxppj7tobEr1mtjm', 'John', 'Doe', 'Male', '28 - 37', 'Italy', 'Management', 'User', 1, 'b5d4e92c18286c8031984793453dc18c', '2017-11-03 08:40:59', 'CREATED'),
	(100, 'sample.user@op.pl', '$2y$10$36Hb7JOhxQ2tx9JmNux9POmuwSq6mV1Z2WWGQzQu1BHE1hXxaN2xq', 'Anna', 'Kraft', 'Female', '38 - 47', 'Germany', 'Production', 'User', 1, 'f43bd04c1af038db8e20e4cd715aab37', '2017-11-03 08:48:17', 'CREATED'),
	(101, 'sample.user2@op.pl', '$2y$10$137HOCn3SkFGMScu/jbA..mhSz5qmKovgsf3kY3/TIQgQDjCXE31W', 'Tom', 'Harris', 'Male', '48 - 57', 'Greece', 'Administration', 'User', 1, '1a9ddb706ae4501999fc892b45285c10', '2017-11-03 08:52:43', 'CREATED'),
	(102, 'sample.user3@op.pl', '$2y$10$fyYT1TIHPs1EtTYgedsiWumUE/.PzzKOt8GL3mmphQr3QYlLgjh86', 'Katarzyna', 'Nowak', 'Female', '58 <', 'Poland', 'Sales', 'User', 1, 'b03e11074b54745f51e9b860cac0fa4c', '2017-11-03 08:58:39', 'CREATED'),
	(103, 'sample.user4@op.pl', '$2y$10$YM7iY5DFCF6VlHJvNXNhQOkxKgIw9wZaAWHynPMo3YH8BD52YhIiO', 'Joan', 'Philips', 'Female', '38 - 47', 'UK', 'Production', 'User', 1, '29ae413a886b289f1519f2638ee1dda5', '2017-11-03 09:02:40', 'CREATED'),
	(104, 'sample.user5@op.pl', '$2y$10$Uod4C.cepEIEVEZ487hBuOhHpJOoquEYfOIvSHqm0U1HMsovN/Rkm', 'Olaf', 'Matz', 'Male', '28 - 37', 'Italy', 'General Management', 'User', 1, '2329faefcee6d3286f705e35c6cd6a64', '2017-11-03 09:08:41', 'CREATED'),
	(105, 'l.k.lapa@gmail.com', '$2y$10$d8zSKY/SjwT0Yj8dSfOvCuOfBhZ5NjJvrl1k3IY1/Ibjw6xYDUuwm', 'aaaaaa', 'aaaaaa', 'Male', '18 - 27', 'Germany', 'Administration', 'User', 0, '3b666c6a92472c119cea22f8625f43c4', '2017-11-07 10:15:06', 'CREATED');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Zrzut struktury tabela inminds_dcnet_eu.user_invited_answers
CREATE TABLE IF NOT EXISTS `user_invited_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `tc_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answers` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli inminds_dcnet_eu.user_invited_answers: ~0 rows (około)
/*!40000 ALTER TABLE `user_invited_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_invited_answers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
