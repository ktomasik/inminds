<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function panelAction()
    {
        return new ViewModel();
    }

    public function evaluationAAction()
    {
        return new ViewModel();
    }

    public function thanksAction()
    {
        return new ViewModel();
    }
    public function langSwitchAction()
    {
        $data = $this->params()->fromPost();
//        echo $data['lang'];
        $session = new Container('lang');
        if(isset($session)){
            unset($_SESSION['lang']);
            $session = new Container('lang');
            $session->lang = $data['lang'];
        } else {
            $session->lang = $data['lang'];
        }
        return new JsonModel();
    }

}
