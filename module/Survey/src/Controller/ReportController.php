<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 11.08.2017
 * Time: 11:10
 */

namespace Survey\Controller;


use Survey\Entity\TrainingCycle;
use Survey\Service\TrainingCycleManager;
use User\Entity\User;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Survey\Entity\SurveyAnswers;
use Survey\Entity\Survey;
use Survey\Form\TrainingCycle as TrainingCycleForm;
use Zend\View\Model\JsonModel;

class ReportController extends AbstractActionController
{
    private $em;

    private $tcManager;

    public function __construct(EntityManager $em, TrainingCycleManager $tcManager)
    {
        $this->em = $em;
        $this->tcManager = $tcManager;
    }

    public function showAction() {
        $tcId = $this->params()->fromRoute('tcId');
        $surveyId = $this->params()->fromRoute('surveyId');
        $identity = $this->identity();
        $user = $this->em->getRepository(User::class)->findOneByLogin($identity);
        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);


        if ($this->params()->fromRoute('surveyId') !== null) {
            $survey_answers = '';
            $surveys_answers = $this->em->getRepository(SurveyAnswers::class)->findBy(['surveyId' => $surveyId, 'userId' => $user->getId()]);
            foreach ($surveys_answers as $sa) {
                $survey_answers = $sa->getAnswer();
            }

            $survey = $this->em->getRepository(Survey::class)->findOneByLevel($surveyId);

            $viewModel = new ViewModel([
                'survey' => $survey,
                'trainingCycle' => $trainingCycle,
                'survey_answers' => $survey_answers,
                'identity' => $identity,
            ]);
            $viewModel->setTemplate('/survey/report/show-answer');
            return $viewModel;
        }
        $surveys = $this->em->getRepository(Survey::class)->findAll();

        return new ViewModel([
            'surveys' => $surveys,
            'trainingCycle' => $trainingCycle,
            'identity' => $identity,
        ]);
    }

    public function tableAction() {
        $tcId = $this->params()->fromRoute('tcId');
        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);

        $survey_answers = $this->em->getRepository(SurveyAnswers::class)->findAll();

        $viewModel = new ViewModel ([
            'survey_answers' => $survey_answers,
            'trainingCycle' => $trainingCycle,
        ]);
        return $viewModel;
    }

    public function chartAction() {
        $userId = $this->identity();
        $currentUser = $this->em->getRepository(User::class)->findOneByLogin($userId);
        /** @var User $currentUser*/
        $tcId = $this->params()->fromRoute('tcId');
        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);

        $survey_answers = $this->em->getRepository(SurveyAnswers::class)->findAll();

        $user_answers = $this->em->getRepository(User::class)->findBy(['id' => $currentUser->getId()]);

        $arr = [];
        foreach ($user_answers as $answer) {
            foreach ($answer->getAnswers() as $userAnswer) {
                $arr[$userAnswer->getSurveyId()][$answer->getId()] = json_decode($userAnswer->getAnswer(), true);
            }
        }


        $viewModel = new ViewModel ([
            'arr' => $arr,
            'survey_answers' => $survey_answers,
            'trainingCycle' => $trainingCycle,

        ]);
        return $viewModel;
    }

    public function chartUserAction() {
        $tcId = $this->params()->fromRoute('tcId');
        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);

        $userId = $this->identity();
        $currentUser = $this->em->getRepository(User::class)->findOneByLogin($userId);
        /** @var User $currentUser*/

        $usersByUserCountry = $this->em->getRepository(User::class)->findBy(['company' => $currentUser->getCompany()]);

        $singleUser = $this->em->getRepository(User::class)->findBy(['company' => $currentUser->getCompany(), 'id' => $currentUser->getId()]);

        $arr = [];

        foreach ($usersByUserCountry as $user) {
            foreach ($user->getAnswers() as $userAnswer) {
//                echo $user->getId() . '  ' . $user->getLogin() . ' ' . $userAnswer->getSurveyId() . PHP_EOL;
                $arr[$userAnswer->getSurveyId()][$user->getId()][] = json_decode($userAnswer->getAnswer(), true);
            }
        }

        $arr2 = [];
        foreach ($singleUser as $answer) {
            foreach ($answer->getAnswers() as $userAnswer) {
                $arr2[$userAnswer->getSurveyId()] = json_decode($userAnswer->getAnswer(), true);
            }
        }

        $viewModel = new ViewModel ([
            'arr' => $arr,
            'trainingCycle' => $trainingCycle,
            'arr2' => $arr2,
        ]);
        return $viewModel;

    }
}
