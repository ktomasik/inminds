<?php
namespace Survey\Controller\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Survey\Service\TrainingCycleManager;
use User\Service\UserManager;
use Zend\Mvc\Application;
use Zend\Mvc\I18n\Translator;
use Zend\ServiceManager\Factory\FactoryInterface;
use Survey\Controller\SurveyController;

class SurveyControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $tcManager = $container->get(TrainingCycleManager::class);
        $userManager = $container->get(UserManager::class);
        $translator = $container->get(Translator::class);

        return new SurveyController($em, $tcManager, $userManager, $translator);
    }

}
