<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 18.08.2017
 * Time: 14:32
 */

namespace Survey\Controller\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Survey\Controller\ReportController;
use Survey\Service\TrainingCycleManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class ReportControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $tcManager = $container->get(TrainingCycleManager::class);

        return new ReportController($em, $tcManager);
    }

}