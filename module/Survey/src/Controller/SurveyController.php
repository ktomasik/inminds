<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Survey\Controller;

use function PHPSTORM_META\type;
use Survey\Entity\Survey;
use Survey\Entity\SurveyAnswers;
use Survey\Entity\Tc2User;
use Survey\Entity\TrainingCycle;
use Survey\Form\TrainingCycle as TrainingCycleForm;
use User\Entity\User;
use User\Service\UserManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\I18n\Translator;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\JsonModel;
use Survey\Service\TrainingCycleManager;
use Survey\Form\InvitationForUser as InvitationForUserForm;

class SurveyController extends AbstractActionController
{
    private $em;

    private $tcManager;

    private $userManager;

    private $translator;

    public function __construct(EntityManager $em, TrainingCycleManager $tcManager, UserManager $userManager, Translator $translator)
    {
        $this->em = $em;
        $this->tcManager = $tcManager;
        $this->userManager = $userManager;
        $this->translator = $translator;

    }

    public function getAction()
    {
        $userId = $this->identity();
        if(!$userId) return $this->redirect()->toRoute('logout');

        $tcId = $this->params()->fromRoute('tcId');

        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);

        $userTc_list = $this->em->getRepository(Tc2User::class)->findBy(array('tcId' => $trainingCycle));

        $identity = $this->identity();

        $surveyLevel = intval($this->params()->fromRoute('surveyId'));
        if ( $surveyLevel !== null && $surveyLevel > 0 && $surveyLevel < 5) {

            $survey = $this->em->getRepository(Survey::class)->findOneByLevel( $surveyLevel );

            if ( json_decode($trainingCycle->getBlocked())[ $surveyLevel - 1 ] == 0 ) {

                $viewModel = new ViewModel([
                    'byInvitation' => SurveyAnswers::FILLED_BY_USER,
                    'survey' => $survey,
                    'trainingCycle' => $trainingCycle,
                    'identity' => $identity,
                ]);
                $viewModel->setTemplate('/survey/survey/get-questions');

                return $viewModel;
            } else
                $this->redirect()->toRoute('questions', array('controller' => 'survey', 'action' => 'get', 'tcId' => $tcId));

        }

        $currentUser = $this->em->getRepository(User::class)->findOneByLogin($identity);
        /** @var User $currentUser*/

        $userByTypeAccount = $this->em->getRepository(User::class)->findBy(['typeAccount' => $currentUser->getTypeAccount()]);
        $arr2 = [];
        foreach ($userByTypeAccount as $users) {
            $arr2[$users->getId()] = $users->getTypeAccount();
        }

        $surveys = $this->em->getRepository(Survey::class)->findAll();
        $usrForm = new InvitationForUserForm();

        return new ViewModel([
            'usersList' => $userTc_list,
            'surveys' => $surveys,
            'trainingCycle' => $trainingCycle,
            'identity' => $identity,
            'arr2' => $arr2,
            'usrForm' => $usrForm,
        ]);
    }

    public function getSurveyQuestionsAction()
    {
        $tcId = $this->params()->fromRoute('tcId');
        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneById($tcId);

        $surveys = $this->em->getRepository(Survey::class)->findAll();

        return new ViewModel([
            'surveys' => $surveys,
            'trainingCycle' => $trainingCycle
        ]);
    }

    public function addNewTcAction()
    {
        $tcForm = new TrainingCycleForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $tcForm->setData($data);
            if ($tcForm->isValid()) {
                // Add user.
                $this->tcManager->addTc($this->identity(), $data);

                return new JsonModel([
                    'success' => true,
                ]);
            } else {
                return new JsonModel([
                    'success' => false,
                    'errors' => $tcForm->getMessages()
                ]);
            }
        }
    }

    public function addNewUsersAction()
    {
        $usrForm = new InvitationForUserForm();

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $usrForm->setData($data);
            if ($usrForm->isValid()) {

                $messages = array();
                $usersField = $usrForm->get('userMail')->getValue();
                $usersMails = explode(PHP_EOL, $usersField);
                $tcId = intval($usrForm->get('tcId')->getValue());

                $tc = $this->em->getRepository(TrainingCycle::class)->findOneBy(array('id' => $tcId));

                foreach ($usersMails as $mail) {
                    $mail = strip_tags(trim($mail));
                    $user = $this->em->getRepository(User::class)->findOneByLogin($mail);

                    if ($user) {
                        $existInDB = $this->em->getRepository(Tc2User::class)->findBy(array('tcId' => $tc, 'userId' => $user));

                        if($existInDB) {
                            array_push($messages, ' ' . $user->getLogin() . ' ' . $this->translator->translate('has been added to this training cycle.'));

                        }else {
                            $newRecord = $this->tcManager->addTc2User($tc, $user);

                            if(isset($newRecord)) {
                                array_push($messages, ' ' . $user->getLogin() . ' ' .$this->translator->translate('added to training cycle.'));
                            }
                            else {
                                    array_push($messages, 'Error during add exist user to training cycle.');
                            }
                        }

                    } else {
                        $data = [
                            'login' => $mail,
                            'password' => bin2hex(openssl_random_pseudo_bytes(8)),
                            'firstname' => '',
                            'lastname' => '',
                            'age' => '',
                            'gender' => '',
                            'company' => '',
                            'position' => '',
                            'type_account' => 'User',
                        ];

                        $new_user = $this->userManager->addUser($data, User::TYPE_INVITED);
                        if($new_user) {
                            $tc2user = $this->tcManager->addTc2User($tc, $new_user);

                            if($tc2user) {
                                array_push($messages, ' ' . $new_user->getLogin() .  ' '.$this->translator->translate('added to training cycle.'));
                            }else {
                                array_push($messages, 'Error during add new user to training cycle.');
                            }
                        } else {
                            array_push($messages, 'Error during create new user.');
                        }
                    }
                }
                $usrForm->setMessages(array('userMail' => $messages));

                return new JsonModel([
                    'success' => true,
                    'errors' => $usrForm->getMessages()
                ]);
            } else
                return new JsonModel([
                    'success' => false,
                    'errors' => $usrForm->getMessages()
                ]);
        }
    }


    public function addSurveyAnswersAction()
    {
        if ($this->getRequest()->isPost()) {
            $identity = $this->identity();

            $user = $this->em->getRepository(User::class)->findOneByLogin($identity);
            $survey = $this->em->getRepository(Survey::class)->findOneById($this->params()->fromPost('surveyId'));

            $dataSurvey = $this->params()->fromPost();
            unset($dataSurvey['tcId']);
            unset($dataSurvey['surveyId']);
            unset($dataSurvey['byInvitation']);

            $data['inputs'] = $dataSurvey;

            $data['byInvitation'] =  $this->params()->fromPost('byInvitation',0);

            if($data['byInvitation'] == SurveyAnswers::FILLED_BY_USER){
                $data['user_id'] = $user->getId();
            } else {
                $data['user_id'] = 0;
            }

            $data['tc_id'] = $this->params()->fromPost('tcId', 0);
            $data['survey'] = $survey;

            $this->tcManager->addSurveyAnswers($data);

            return new JsonModel([
                'success' => true,
                'byInvitation' => true
            ]);
        }
    }

    public function invitationAction()
    {
        $tcId = $this->params()->fromRoute('tcId');
        $invitation = $this->params()->fromRoute('invitation');
        $level = $this->params()->fromRoute('level');

        $identity = $this->identity();

        if($invitation[0] == 'm') {
            $invitationColumn = 'm_invitation';
        } else {
            $invitationColumn = 'c_invitation';
        }

        $trainingCycle = $this->em->getRepository(TrainingCycle::class)->findOneBy([
            'id' => $tcId,
            $invitationColumn => $invitation
        ]);

        $survey = $this->em->getRepository(Survey::class)->findOneByLevel($level);

        $viewModel = new ViewModel([
            'byInvitation' => SurveyAnswers::FILLED_VIA_INVITATION,
            'survey' => $survey,
            'trainingCycle' => $trainingCycle,
            'identity' => $identity
        ]);
        $viewModel->setTemplate('/survey/survey/get-questions');

        return $viewModel;

    }

    public function unblockAction(){

        $result = false;
        $message = '';
        if($this->getRequest()->isPost('id') && $this->getRequest()->isPost('tcid')) {
            $id = strip_tags(trim($this->getRequest()->getPost('id')));
            $tcId = strip_tags(trim($this->getRequest()->getPost('tcid')));

            $tc = $this->em->getRepository(TrainingCycle::class)->findOneBy(array('id' => $tcId));
            $blocked_value = json_decode($tc->getBlocked());
            if($blocked_value[$id] == 1) {
                $blocked_value[$id] = 0;
                $blocked_value = json_encode($blocked_value);
                $tc->setBlocked($blocked_value);
                $this->em->persist($tc);
                $this->em->flush();
            }
            unset($tc);

            switch ($id) {
                case 0: $message = 'First Survey is unblocked.'; break;
                case 1: $message = 'Second Survey is unblocked.'; break;
                case 2: $message = 'Third Survey is unblocked.'; break;
                case 3: $message = 'Fourth Survey is unblocked.'; break;
            }
            $result = true;
        }

        return new JsonModel([
            'success' => $result,
            'message' => $message
        ]);
    }
}
