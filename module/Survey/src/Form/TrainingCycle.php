<?php

namespace Survey\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class TrainingCycle extends Form
{
    // Constructor.
    public function __construct()
    {
        // Define form name
        parent::__construct('tc-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
//        $this->setAttribute('action', '/login');
        $this->setAttribute('class', 'form-horizontal');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    // This method adds elements to form (input fields and
    // submit button).
    private function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'description',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Description',
            ],
        ]);

        $this->add([
//            'type' => 'text',
            'name' => 'dateFrom',
            'attributes' => [
                'class' => 'form-control datepicker',
                'data-date-format' => 'mm/dd/yyyy'

            ],
            'options' => [
                'label' => 'From date',
            ],
        ]);

        $this->add([
//            'type' => 'text',
            'name' => 'dateTo',
            'attributes' => [
                'class' => 'form-control datepicker',
                'data-date-format' => 'mm/dd/yyyy'
            ],
            'options' => [
                'label' => 'To date',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [],
        ]);

        $inputFilter->add([
            'name' => 'dateFrom',
            'required' => true,
        ]);

        $inputFilter->add([
            'name' => 'dateTo',
            'required' => true,
        ]);
    }
}
