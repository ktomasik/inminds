<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 05.09.2017
 * Time: 09:41
 */

namespace Survey\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class InvitationForUser extends Form
{
    // Constructor.
    public function __construct()
    {
        // Define form name
        parent::__construct('usr-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type' => 'textarea',
            'name' => 'userMail',
            'attributes' => [
                'class' => 'form-control input-md',
                'id' => 'userMail'
            ],
            'options' => [
                'label' => 'Users E-mail',
            ],
        ]);
        $this->add([
            'type' => 'hidden',
            'name' => 'tcId',

        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'userMail',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [],
        ]);
        $inputFilter->add([
            'name' => 'tcId',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [],
        ]);
    }
}