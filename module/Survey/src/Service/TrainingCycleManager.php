<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-13
 * Time: 08:27
 */

namespace Survey\Service;

use Doctrine\ORM\EntityManager;
use Survey\Entity\SurveyAnswers;
use Survey\Entity\TrainingCycle;
use User\Entity\User;
use Survey\Entity\Tc2User;

/**
 * This service is responsible for adding/editing training cycles.
 */
class TrainingCycleManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Constructs the service.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * This method adds a new training cycle.
     * @param $data array
     * @param $identity string
     * @throws \Exception
     * @return mixed
     */
    public function addTc($identity, $data)
    {
        $tc = new TrainingCycle();
        $user = $this->em->getRepository(User::class)->findOneByLogin($identity);

        $tc->setName($data['name']);
        $tc->setUser($user);
        $tc->setDescription($data['description']);
        $tc->setDateFrom(new \DateTime($data['dateFrom']));
        $tc->setDateTo(new \DateTime($data['dateTo']));
        $tc->setMInvitation(uniqid('m'));
        $tc->setCInvitation(uniqid('c'));
        $tc->setBlocked('[1,1,1,1]');

        // Add the entity to the entity manager.
        $this->em->persist($tc);

        // Apply changes to database.
        $this->em->flush();

        return $tc;
    }

    public function addTc2User($tc, $user)
    {
        $u2t = new Tc2User();

        $u2t->setTc($tc);
        $u2t->setUser($user);

        $this->em->persist($u2t);
        $this->em->flush();

        return $u2t;
    }

    public function addSurveyAnswers($data)
    {
        $answers = new SurveyAnswers();

        $user = $this->em->getRepository(User::class)->findOneById($data['user_id']);

        $answers->setUser($user);
        $answers->setAnswer(json_encode($data['inputs']));
        $answers->setSurvey($data['survey']);
        $answers->setTcId($data['tc_id']);
        $answers->setFilledViaInvitation($data['byInvitation']);

        // Add the entity to the entity manager.
        $this->em->persist($answers);

        // Apply changes to database.
        $this->em->flush();

        return $answers;
    }

    public function updateSurveyAnswers($data)
    {
        $answers = $this->em->getRepository(SurveyAnswers::class)->findOneById($data['survey_answers_id']);
        $answers->setAnswer(json_encode($data['inputs']));

        // Apply changes to database.
        $this->em->flush();

        return true;
    }

}
