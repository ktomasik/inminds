<?php

namespace Survey\Service\Factory;

use Interop\Container\ContainerInterface;
use Survey\Service\TrainingCycleManager;

/**
 * This is the factory class for TrainingCycleManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class TrainingCycleManagerFactory
{
    /**
     * This method creates the UserManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get('doctrine.entitymanager.orm_default');

        return new TrainingCycleManager($em);
    }
}
