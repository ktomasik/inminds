<?php
namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="training_cycle")
 */
class TrainingCycle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @ORM\Column(name="date_from", type="date")
     */
    protected $date_from;

    /**
     * @ORM\Column(name="date_to", type="date")
     */
    protected $date_to;

    /**
     * @ORM\Column(name="c_invitation", type="text")
     */
    protected $c_invitation;

    /**
     * @ORM\Column(name="m_invitation", type="text")
     */
    protected $m_invitation;

    /**
     * @ORM\ManyToOne(targetEntity="\User\Entity\User", inversedBy="trainingcycle")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    /**
     * @ORM\Column(name="blocked", type="text")
     */
    protected $blocked;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDateFrom()
    {
        return (string)$this->date_from->format('Y-m-d');
    }

    /**
     * @param mixed $date_from
     */
    public function setDateFrom($date_from)
    {
        $this->date_from = $date_from;
    }

    /**
     * @return mixed
     */
    public function getDateTo()
    {
        return (string)$this->date_to->format('Y-m-d');
    }

    /**
     * @param mixed $date_to
     */
    public function setDateTo($date_to)
    {
        $this->date_to = $date_to;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCInvitation()
    {
        return $this->c_invitation;
    }

    /**
     * @param mixed $c_invitation
     */
    public function setCInvitation($c_invitation)
    {
        $this->c_invitation = $c_invitation;
    }

    /**
     * @return mixed
     */
    public function getMInvitation()
    {
        return $this->m_invitation;
    }

    /**
     * @param mixed $m_invitation
     */
    public function setMInvitation($m_invitation)
    {
        $this->m_invitation = $m_invitation;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @return mixed $blocked
     */
    public function setBlocked($blocked)
    {
        return $this->blocked = $blocked;
    }

}
