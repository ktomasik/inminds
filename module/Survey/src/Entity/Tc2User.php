<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 05.09.2017
 * Time: 09:16
 */

namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="tc2user")
 */
class Tc2User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="tc_id", type="integer")
     */
    protected $tcId;


    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Entity\TrainingCycle", inversedBy="tc2user")
     * @ORM\JoinColumn(name="tc_id", referencedColumnName="id")
     */
    protected $tc;


    /**
     * @ORM\ManyToOne(targetEntity="\User\Entity\User", inversedBy="tc2user")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTcId()
    {
        return $this->tcId;
    }

    /**
     * @param mixed $tcId
     */
    public function setTcId($tcId)
    {
        $this->tcId = $tcId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getTc()
    {
        return $this->tc;
    }

    /**
     * @param mixed $tc
     */
    public function setTc($tc)
    {
        $this->tc = $tc;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }

}