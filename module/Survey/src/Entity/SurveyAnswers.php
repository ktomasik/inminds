<?php
namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="survey_answers")
 */
class SurveyAnswers
{
    const FILLED_BY_USER = 0;
    const FILLED_VIA_INVITATION = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(name="survey_id", type="integer")
     */
    protected $surveyId;

    /**
     * @ORM\Column(name="tc_id", type="integer")
     */
    protected $tcId;

    /**
     * @ORM\Column(name="answer", type="string")
     *
     */
    protected $answer;

    /**
     * @ORM\Column(name="filled_via_invitation", type="string")
     *
     */
    protected $filled_via_invitation;

    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Entity\Survey", inversedBy="surveyanswers")
     * @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     */
    protected $survey;

    /**
     * @ORM\ManyToOne(targetEntity="\User\Entity\User", inversedBy="surveyanswers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param mixed $surveyId
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;
    }

    /**
     * @return mixed
     */
    public function getTcId()
    {
        return $this->tcId;
    }

    /**
     * @param mixed $tcId
     */
    public function setTcId($tcId)
    {
        $this->tcId = $tcId;
    }

    /**
     * @return mixed
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * @param mixed $survey
     */
    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return mixed
     */
    public function getFilledViaInvitation()
    {
        return $this->filled_via_invitation;
    }

    /**
     * @param mixed $filled_via_invitation
     */
    public function setFilledViaInvitation($filled_via_invitation)
    {
        $this->filled_via_invitation = $filled_via_invitation;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}
