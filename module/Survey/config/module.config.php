<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Survey;

use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'survey' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/survey[/:action]',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action' => 'get',
                    ],
                ],
            ],
            'questions' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/survey/get[/:tcId[/:surveyId]]',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action' => 'get',
                    ],
                ],
            ],
            'invitation' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/survey/invitation[/:tcId][/:level][/:invitation]',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action' => 'invitation',
                    ],
                ],
            ],
            'report' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report[/:action]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'show',
                    ],
                ],
            ],
            'answers' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report/show[/:tcId[/:surveyId]]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'show',
                    ],
                ],
            ],
            'tables' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report/table[/:tcId]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'table',
                    ],
                ],
            ],
            'charts' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report/chart[/:tcId]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'chart',
                    ],
                ],
            ],
            'country' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report/chartUser[/:tcId]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'chartUser',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\SurveyController::class => Controller\Factory\SurveyControllerFactory::class,
            Controller\ReportController::class => Controller\Factory\ReportControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\TrainingCycleManager::class => Service\Factory\TrainingCycleManagerFactory::class
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
];
