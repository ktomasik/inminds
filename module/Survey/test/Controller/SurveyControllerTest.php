<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-30
 * Time: 10:42
 */

namespace SurveyTest\Controller;

use Survey\Controller\SurveyController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class SurveyControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/survey', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Survey');
        $this->assertControllerName(SurveyController::class);
        $this->assertControllerClass('SurveyController');
        $this->assertMatchedRouteName('survey');
    }

    public function testInvalidRouteDoesNotCrash()
    {
        $this->dispatch('/survey/invalid/route', 'GET');
        $this->assertResponseStatusCode(404);
    }
}
