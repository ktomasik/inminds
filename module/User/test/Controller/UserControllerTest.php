<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-30
 * Time: 10:42
 */

namespace UserTest\Controller;

use User\Controller\UserController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class UserControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/user', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('user');
        $this->assertControllerName(UserController::class); // as specified in router's controller name alias
        $this->assertControllerClass('UserController');
        $this->assertMatchedRouteName('user');
    }

    public function testInvalidRouteDoesNotCrash()
    {
        $this->dispatch('/user/invalid/route', 'GET');
        $this->assertResponseStatusCode(404);
    }
}
