<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-21
 * Time: 10:19
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class RegistrationForm extends Form
{
    // Constructor.
    public function __construct()
    {
        // Define form name
        parent::__construct('registration-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
//        $this->setAttribute('action', '/user/add');
        $this->setAttribute('class', 'form-horizontal');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    // This method adds elements to form (input fields and
    // submit button).
    private function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Your E-mail',
            ],
        ]);

        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Password',
            ],
        ]);

        $this->add([
            'type' => 'password',
            'name' => 're-password',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Re-type password',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'firstname',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'First name',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'lastname',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Last name',
            ],
        ]);

        $this->add([
            'type' => 'select',
            'name' => 'age',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Age',
                'value_options' => [
                    ' ' => 'Please select',
                    '18 - 27' => '18 - 27',
                    '28 - 37' => '28 - 37',
                    '38 - 47' => '38 - 47',
                    '48 - 57' => '48 - 57',
                    '58 <' => '58 and more',
                ],
            ],
        ]);

        $this->add([
            'type' => 'select',
            'name' => 'gender',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Gender',
                'value_options' => [
                    ' ' => 'Please select',
                    'Male' => 'Male',
                    'Female' => 'Female',
                ],
            ],
        ]);

        $this->add([
            'type' => 'select',
            'name' => 'company',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Country',
                'value_options' => [
                    ' ' => 'Please select',
                    'Germany' => 'Germany',
                    'Greece' => 'Greece',
                    'Italy' => 'Italy',
                    'Poland' => 'Poland',
                    'UK' => 'UK',
                ],
            ],
        ]);

        $this->add([
            'type' => 'select',
            'name' => 'position',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Position',
                'value_options' => [
                    ' ' => 'Please select',
                    'Administration' => 'Administration',
                    'General Management' => 'General Management',
                    'Management' => 'Management',
                    'Production' => 'Production',
                    'Sales' => 'Sales',
                ],
            ],
        ]);

        $this->add([
            'type' => 'select',
            'name' => 'type_account',
            'attributes' => [
                'class' => 'form-control input-md',
            ],
            'options' => [
                'label' => 'Type account',
                'value_options' => [
                    ' ' => 'Please select',
                    'Manager' => 'Manager',
                    'User' => 'User',
                ],
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'login',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'required' => true,
                    'options' => [
//                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
//                        'useMxCheck' => false,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 4,
                        'max' => 64
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 're-password',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Identical',
                    'options' => [
                        'min' => 4,
                        'max' => 64,
                        'token' => 'password'
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'firstname',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'lastname',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'age',
            'required' => true,
            'filters' => []
        ]);

        $inputFilter->add([
            'name' => 'gender',
            'required' => true,
            'filters' => []
        ]);

        $inputFilter->add([
            'name' => 'company',
            'required' => true,
            'filters' => []
        ]);

        $inputFilter->add([
            'name' => 'position',
            'required' => true,
            'filters' => []
        ]);

        $inputFilter->add([
            'name' => 'type_account',
            'required' => true,
            'filters' => []
        ]);
    }
}
