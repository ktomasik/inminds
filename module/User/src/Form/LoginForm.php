<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-21
 * Time: 10:18
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class LoginForm extends Form
{
    // Constructor.
    public function __construct()
    {
        // Define form name
        parent::__construct('login-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
//        $this->setAttribute('action', '/login');
        $this->setAttribute('class', 'form-horizontal');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    // This method adds elements to form (input fields and
    // submit button).
    private function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'class' => 'form-control input-md',
//                'placeholder' => 'Your email'
            ],
            'options' => [
                'label' => 'Your E-mail',
            ],
        ]);

        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'class' => 'form-control input-md',
//                'placeholder' => 'Your password'
            ],
            'options' => [
                'label' => 'Password',
            ],
        ]);

        $this->add([
            'type' => 'checkbox',
            'name' => 'remember_me',
            'attributes' => [
                'class' => 'form-check-input'
            ],
            'options' => [
                'label' => 'Remember Me',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'login',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);
    }
}
