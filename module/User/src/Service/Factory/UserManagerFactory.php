<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-07
 * Time: 13:16
 */

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\UserManager;

/**
 * This is the factory class for UserManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class UserManagerFactory
{
    /**
     * This method creates the UserManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get('doctrine.entitymanager.orm_default');
        $config = $container->get('config');

        return new UserManager($em, $config);
    }
}
