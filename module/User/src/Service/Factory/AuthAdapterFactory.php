<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-07
 * Time: 13:14
 */

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\AuthAdapter;
use Zend\Mvc\I18n\Translator;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class AuthAdapterFactory implements FactoryInterface
{
    /**
     * This method creates the AuthAdapter service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Get Doctrine entity manager from Service Manager.
        $em = $container->get('doctrine.entitymanager.orm_default');
        $translator = $container->get(Translator::class);

        // Create the AuthAdapter and inject dependency to its constructor.
        return new AuthAdapter($em, $translator);
    }
}
