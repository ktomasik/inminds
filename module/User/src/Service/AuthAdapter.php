<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-07
 * Time: 12:42
 */

namespace User\Service;

use Zend\Authentication\Adapter\AdapterInterface;
use Doctrine\ORM\EntityManager;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;
use User\Entity\User;
use Zend\Mvc\I18n\Translator;

/**
 * Class AuthAdapter
 * @package User\Service
 */
class AuthAdapter implements AdapterInterface
{
    /**
     * User login.
     * @var string
     */
    private $login;

    /**
     * Password
     * @var string
     */
    private $password;

    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * AuthAdapter constructor.
     * @param EntityManager $em
     */

    private $translator;
    public function __construct(EntityManager $em, Translator $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * Sets user email.
     * @var string
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * Sets password.
     * @var string
     */
    public function setPassword($password)
    {
        $this->password = (string)$password;
    }

    /**
     * Performs an authentication attempt.
     */
    public function authenticate($autoAuthenticate = false)
    {
        // Check the database if there is a user with such email.
        /** @var \User\Entity\User */
        $user = $this->em->getRepository(User::class)
            ->findOneByLogin($this->login);

        // If there is no such user, return 'Identity Not Found' status.
        if ($user == null) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                null,
                [$this->translator->translate('Invalid credentials.')]);
        }

        // If the user with such email exists, we need to check if it is active or retired.
        // Do not allow retired users to log in.
        if ($user->getStatus() == User::STATUS_INACTIVE) {
            return new Result(
                Result::FAILURE,
                null,
                [$this->translator->translate('Account is not activated.')]);
        }

        // Now we need to calculate hash based on user-entered password and compare
        // it with the password hash stored in database.
        $bcrypt = new Bcrypt();
        $passwordHash = $user->getPassword();

        if ($bcrypt->verify($this->password, $passwordHash)) {
            // Great! The password hash matches. Return user identity (login) to be
            // saved in session for later use.
            return new Result(
                Result::SUCCESS,
                $this->login,
                [$this->translator->translate('Authenticated successfully.')]);
        }

        // If password check didn't pass return 'Invalid Credential' failure status.
        return new Result(
            Result::FAILURE_CREDENTIAL_INVALID,
            null,
            [$this->translator->translate('Invalid credentials.')]);
    }
}
