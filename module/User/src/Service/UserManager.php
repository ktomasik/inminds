<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-07
 * Time: 13:00
 */

namespace User\Service;

use Doctrine\ORM\EntityManager;
use User\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Sql\Ddl\Column\Datetime;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;


/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class UserManager
{
    //for mail
    const ACTIVATE_CODE = 1;
    const PASSWORD = 2;
    const NEW_PASSWORD = 3;
    const INVITED = 4;



    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $config;

    /**
     * Constructs the service.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    private function sendMail(User $user, $type, $password=null){

        $isSSL = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != '' ? "https" : "http";
        $message = new Message();

        switch ($type) {
            case 1:
                $html = new MimePart('</div>
                                                <img src="'.$isSSL.'://'.$_SERVER['SERVER_NAME'].'/img/inminds_logo.jpg" style="width: 35%;">
                                                <div style="margin-top: 25px;">
                                                    <p style="font-size: 17px;">You were invited to create account in Inminds Project. To activate your account please click follow link: <a href="http://'.$_SERVER['SERVER_NAME'].'/activate?hash='.$user->getActivationHash(). '">activation link</a></p>
                                                </div>
                                                <div style="margin-top: 60px;">
                                                    <img src="'.$isSSL.'://'.$_SERVER['SERVER_NAME'].'/img/eu_flag_erasmusmu.png" style="float: left"><p style="font-size: 13px; position: relative; top:25px;">Co-funded by the Erasmus+ Programme of the European Union. This publication reflects the views only of the author, and the Commission cannot be held responsible for any use which may be made of the information contained therein.</p>
                                                </div>');
                $html->type = "text/html";

                $body = new MimeMessage();
                $body->addPart($html);
                $message->setBody($body);
                break;
            case 2:
                $html = new MimePart('</div>
                                                <img src="'.$isSSL.'://'.$_SERVER['SERVER_NAME'].'/img/inminds_logo.jpg" style="width: 30%;">
                                                <div style="margin-top: 25px;">
                                                    <p style="font-size: 17px;">Your password to Inminds account:<strong> '.$password.'</strong></a></p>
                                                </div>
                                                <div style="margin-top: 60px;">
                                                    <img src="'.$isSSL.'://'.$_SERVER['SERVER_NAME'].'/img/eu_flag_erasmusmu.png" style="float: left; width: 30%"><p style="font-size: 13px; position: relative; top:25px;">Co-funded by the Erasmus+ Programme of the European Union. This publication reflects the views only of the author, and the Commission cannot be held responsible for any use which may be made of the information contained therein.</p>
                                                </div>');
                $html->type = "text/html";

                $body = new MimeMessage();
                $body->addPart($html);
                $message->setBody($body);
                break;
            case 3:
                $message->setBody('reset password');
                break;
            case 4:
                $message->setBody('Invited to new TC!');
                break;
            default:
                die();
                break;
        }

        $message->setFrom('inminds2017@outlook.com');
        $message->addTo($user->getLogin());
        $message->setSubject('New account in InMinds Platform');

        $transport = new SmtpTransport();

        $options = new SmtpOptions($this->config['mail']);
        $transport->setOptions($options);
        $transport->send($message);

    }

    /**
     * This method adds a new user.
     * @param $data array
     * @throws \Exception
     * @return \User\Entity\User | bool
     */
    public function addUser($data, $type = null)
    {
//         Do not allow several users with the same email address.
        if ($this->checkUserExists($data['login'])) {
            return false;
        }

        // Create new User entity.
        $user = new User();
        $user->setLogin($data['login']);
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        $user->setAge($data['age']);
        $user->setGender($data['gender']);
        $user->setCompany($data['company']);
        $user->setPosition($data['position']);
        $user->setTypeAccount($data['type_account']);

        // Encrypt password and store the password in encrypted state.
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create($data['password']);
        $user->setPassword($passwordHash);

        $user->setStatus(USER::STATUS_INACTIVE);

        $currentDate = date('Y-m-d H:i:s');
        $user->setDateCreated($currentDate);

        switch ($type){
            case User::TYPE_INVITED: $user->setLastLogin(User::TYPE_INVITED); break;
            case null: $user->setLastLogin(User::TYPE_CREATED); break;
        }

        $user->setActivationHash(md5($data['login']));
        // Add the entity to the entity manager.
        $this->em->persist($user);

        // Apply changes to database.
        $this->em->flush();

        //Send mail with activation code
        $this->sendMail($user, UserManager::ACTIVATE_CODE);
        //var_dump($data['password']);
        $this->sendMail($user, UserManager::PASSWORD, $data['password']);

        return $user;
    }

    /**
     * This method updates data of an existing user.
     */
    public function updateUser(User $user, $data)
    {
//        // Do not allow to change user email if another user with such email already exits.
//        if ($user->getLogin() != $data['login'] && $this->checkUserExists($data['login'])) {
//            throw new \Exception("Another user with email address " . $data['login'] . " already exists");
//        }

        //$user->setLogin($data['login']);
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        //$user->setStatus($data['status']);
        $user->setGender($data['gender']);
        $user->setAge($data['age']);
        $user->setCompany($data['company']);
        $user->setPosition($data['position']);
        $dateObj =  new \DateTime('NOW');
        $date = $dateObj->format('Y-m-d H:i:s');
        $user->setLastLogin($date);

        // Apply changes to database.
        $this->em->flush();
        return true;
    }

    /**
     * Checks whether an active user with given email address already exists in the database.
     * @param $login string
     * @return bool
     */
    public function checkUserExists($login)
    {

        $user = $this->em->getRepository(User::class)
            ->findOneByLogin($login);

        return $user !== null;
    }

    /**
     * Checks that the given password is correct.
     */
    public function validatePassword(User $user, $password)
    {
        $bcrypt = new Bcrypt();
        $passwordHash = $user->getPassword();

        if ($bcrypt->verify($password, $passwordHash)) {
            return true;
        }

        return false;
    }

    public function activateAccount($hash) {
        $user = $this->em->getRepository(User::class)
            ->findOneByActivationHash($hash);

        if ($user !== null && $user->getStatus() == User::STATUS_INACTIVE) {
            $user->setStatus(User::STATUS_ACTIVE);

            // Apply changes to database.
            $this->em->flush();

            return true;
        } else {
            return false;
        }
    }


}
