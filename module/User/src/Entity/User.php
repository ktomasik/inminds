<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-18
 * Time: 12:19
 */

namespace User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    // User status constants.
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';


    const TYPE_CREATED = 'CREATED';
    const TYPE_INVITED = 'INVITED';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="login", type="string")
     */
    protected $login;

    /**
     * @ORM\Column(name="password", type="string")
     */
    protected $password;

    /**
     * @ORM\Column(name="firstname", type="string")
     */
    protected $firstname;

    /**
     * @ORM\Column(name="lastname", type="string")
     */
    protected $lastname;

    /**
     * @ORM\Column(name="gender", type="string")
     */
    protected $gender;

    /**
     * @ORM\Column(name="age", type="string")
     */
    protected $age;

    /**
     * @ORM\Column(name="company", type="string")
     */
    protected $company;

    /**
     * @ORM\Column(name="position", type="string")
     */
    protected $position;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(name="activation_hash", type="string")
     */
    protected $activationHash;

    /**
     * @ORM\Column(name="date_created", type="string")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(name="last_login", type="string")
     */
    protected $lastLogin;

    /**
     * @ORM\Column(name="type_account", type="string")
     */
    protected $typeAccount;

    /**
     * One manager have many training cycles.
     * @ORM\OneToMany(targetEntity="\Survey\Entity\TrainingCycle",  mappedBy="user")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id")
     */
    protected $tc;

    /**
     * One survey has many answers.
     * @ORM\OneToMany(targetEntity="\Survey\Entity\SurveyAnswers",  mappedBy="user")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id")
     */
    protected $answers;

    public function __construct()
    {
        $this->tc = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getActivationHash()
    {
        return $this->activationHash;
    }

    /**
     * @param string $activationHash
     */
    public function setActivationHash($activationHash)
    {
        $this->activationHash = $activationHash;
    }


    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return string
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param string $lastLogin
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param integer $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getTypeAccount()
    {
        return $this->typeAccount;
    }

    /**
     * @param string $type_account
     */
    public function setTypeAccount($type_account)
    {
        $this->typeAccount = $type_account;
    }

    /**
     * @return mixed
     */
    public function getTc()
    {
        return $this->tc;
    }

    /**
     * @return mixed
     */
    public function getAnswers()
    {
        return $this->answers;
    }

}
