<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-11-18
 * Time: 13:01
 */

namespace User\Controller\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use User\Service\AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use User\Controller\UserController;
use User\Service\UserManager;

class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $userManager = $container->get(UserManager::class);

        return new UserController($em, $userManager);
    }

}
