<?php
/**
 * Created by PhpStorm.
 * User: spietraszek
 * Date: 2016-12-07
 * Time: 13:20
 */

namespace User\Controller;

use Doctrine\ORM\EntityManager;
use User\Entity\User;
use User\Form\LoginForm;
use User\Form\RegistrationForm;
use User\Service\UserManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;

/**
 * This controller is responsible for letting the user to log in and log out.
 */
class AuthController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Auth manager.
     * @var \User\Service\AuthManager
     */
    private $authManager;

    /**
     * Auth service.
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * User manager.
     * @var \User\Service\UserManager
     */
    private $userManager;

    /**
     * Constructor.
     */
    public function __construct(EntityManager $em, $authManager, $authService, $userManager)
    {
        $this->em = $em;
        $this->authManager = $authManager;
        $this->authService = $authService;
        $this->userManager = $userManager;
    }

    /**
     * Authenticates user given email address and password credentials.
     */
    public function loginAction()
    {
        $loginForm = new LoginForm();
        $registrationForm = new RegistrationForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();

            $loginForm->setData($data);

            if ($loginForm->isValid()) {
                // Perform login attempt.
                $result = $this->authManager->login($data['login'], $data['password'], ($data['remember_me'] == 1 ? true : false));

                // Check result.
                if ($result->getCode() == Result::SUCCESS) {

                    return new JsonModel([
                        'success' => true
                    ]);
                } else {
                    return new JsonModel([
                        'success' => false,
                        'errors' => $result->getMessages()
                    ]);
                }
            } else {
                return new JsonModel([
                    'success' => false,
                    'errors' => $loginForm->getMessages()
                ]);
            }
        }

        return new ViewModel([
            'loginForm' => $loginForm,
            'registrationForm' => $registrationForm
        ]);
    }

    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction()
    {
        $this->authManager->logout();

        return $this->redirect()->toRoute('login');
    }

    public function activateAction(){

        $data = strip_tags(trim($this->getRequest()->getQuery('hash')));
        if($data) {

            $result = $this->userManager->activateAccount($data);
            return new ViewModel(array( 'result' => $result ));

        } else {
            $this->redirect()->toRoute('login');
        }
    }
}
