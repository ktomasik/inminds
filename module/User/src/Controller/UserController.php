<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Survey\Form\TrainingCycle as TrainingCycleForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use User\Entity\User;
use User\Form\RegistrationForm;
use Survey\Form\InvitationForUser as InvitationForUserForm;
use Survey\Entity\Tc2User;
use Survey\Entity\SurveyAnswers;

class UserController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * User manager.
     * @var \User\Service\UserManager
     */
    private $userManager;

    public function __construct(EntityManager $em, $userManager)
    {
        $this->em = $em;
        $this->userManager = $userManager;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function addAction()
    {
        $registrationForm = new RegistrationForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $registrationForm->setData($data);
            if ($registrationForm->isValid()) {
                //check if user exist
                if (!$this->userManager->addUser($data)){
                    return new JsonModel([
                        'success' => false,
                        'messages' => "User with email address " . $data['login'] . " already exists"
                    ]);
                }

                // Redirect to "profile" page
                return new JsonModel([
                    'success' => true
                ]);

            } else {
                if($this->userManager->checkUserExists($data['login'])) {
                    return new JsonModel([
                        'success' => false,
                        'messages' => "User with email address " . $data['login'] . " already exists"
                    ]);
                }
                return new JsonModel([
                    'success' => false,
                    'errors' => $registrationForm->getMessages()
                ]);
            }
        }

        return new ViewModel();
    }

    public function profileAction()
    {
        if($this->getRequest()->isPost('action') == 'update') {

            $user = $this->em->getRepository(User::class)->findOneByLogin($this->identity());
            $data = array();
            parse_str($this->params()->fromPost('data'), $data);
            $form = new RegistrationForm();
            $form->setValidationGroup(array(
                    'firstname',
                    'lastname',
                    'age',
                    'gender',
                    'company',
                    'position'
                )
            );
            $form->setData($data);
            if($form->isValid()) {
                $res = $this->userManager->updateUser($user, $data);
                if($res)
                    return new JsonModel(['success' => true]);
                else
                    return new JsonModel(['success' => false]);
            }
            else
                return new JsonModel(['success' => false]);

        }

        $tcForm = new TrainingCycleForm();
        $user = $this->em->getRepository(User::class)->findOneByLogin($this->identity());

        return new ViewModel([
            'user' => $user,
            'tcForm' => $tcForm,
        ]);
    }

    public function trainingCycleAction()
    {
        $account_created_by_invited = $this->em->getRepository(User::class)->findOneByLogin($this->identity())->getLastLogin();
        if($account_created_by_invited === 'INVITED')
            $this->redirect()->toRoute('user', array('controller' => 'user', 'action' => 'fillProfile'));


        $tcForm = new TrainingCycleForm();
        $usrForm = new InvitationForUserForm();

        $user = $this->em->getRepository(User::class)->findOneByLogin($this->identity());

        $userId = $this->identity();
        $currentUser = $this->em->getRepository(User::class)->findOneByLogin($userId);
        /** @var User $currentUser*/
        $usr = $this->em->getRepository(Tc2User::class)->findBy(['user' => $currentUser]);

        $userByTypeAccount = $this->em->getRepository(User::class)->findBy(['typeAccount' => $currentUser->getTypeAccount()]);
        $arr2 = [];
        foreach ($userByTypeAccount as $users) {
            $arr2[$users->getId()] = $users->getTypeAccount();
        }

        return new ViewModel([
            'user' => $user,
            'tcForm' => $tcForm,
            'arr2' => $arr2,
            'usrForm' => $usrForm,
            'usr' => $usr,
        ]);
    }

    public function fillProfileAction()
    {
        $account_created_by_invited = $this->em->getRepository(User::class)->findOneByLogin($this->identity())->getLastLogin();
        if($account_created_by_invited !== 'INVITED')
            $this->redirect()->toRoute('user', array('controller' => 'user', 'action' => 'user', 'action' => 'trainingCycle'));

        $user = $this->em->getRepository(User::class)->findOneByLogin($this->identity());
        $profile_form = new RegistrationForm();

        return new ViewModel([
            'profile_form' => $profile_form,
            'user' => $user
        ]);
    }

}
